package net.nextpulse.sparkadmin.exceptions;

/**
 * Exception thrown when a referenced resource or page could not be found.
 *
 * @author yholkamp
 */
public class NotFoundException extends Exception {
}
