package net.nextpulse.sparkadmin;

/**
 *
 *
 * @author yholkamp
 */
public enum ColumnType {
  string,
  text,
  integer,
  bool,
  datetime;

}
